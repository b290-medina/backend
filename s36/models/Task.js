const mongoose = require("mongoose");

// Create the Schema, model and export the file
const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default: "pending"
	}
});


module.exports = mongoose.model("Task", taskSchema);
// "module.exports" is a way for Node JS to treat this value as a "package" that can be used by other files
