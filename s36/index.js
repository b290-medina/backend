// Setting up dependencies
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute.js")

// Server setup
const app = express();
const port = 4000;

// Database connection

mongoose.connect("mongodb+srv://admin:admin123@zuitt.7kyra1x.mongodb.net/B290-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true, 
		useUnifiedTopology: true
	}
);

let db = mongoose.connection
db.on("error",console.error.bind(console, "DB Connection error"));
db.once("open", ()=>console.log("MongoDB Atlas connected!"));


// Setup middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Setup routes
app.use("/tasks", taskRoute);




if(require.main===module){
	app.listen(port,()=>console.log(`Server running at ${port}`))
}
module.exports = app;