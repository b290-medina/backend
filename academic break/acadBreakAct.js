
// ================================================================== 1st item
function shipItem(item, weight, location){
    let totalShippingFee = 0;
    if(weight<=3){
        totalShippingFee+=150;
    }else if(weight>3&&weight<=5){
        totalShippingFee+=280;
    }else if(weight>5&&weight<=8){
        totalShippingFee+=380;
    }else if(weight>8&&weight<=10){
        totalShippingFee+=410;
    }else if(weight>10){
        totalShippingFee+=560;
    }else{
        return `Item ${weight} must be valid!`;
    }
    switch(location){
        case "local":
            totalShippingFee+=0;
            break;
        case "international":
            totalShippingFee+=250;
            break;
        default:
            return `Please input a valid location.`;
    }
    return `The total shipping fee for an ${item} that will ship ${location} is ${totalShippingFee}`
};

console.log(shipItem("moisturizer",8,"local"));

// ================================================================== 2nd item

function Product(name, price, quantity){
        this.name = name;
        this.price=price;
        this.quantity=quantity;
}

// Creating an instance of products
let product1 = new Product("Moisturizer1", 50, 1);
let product2 = new Product("Moisturizer2", 51, 1);
let product3 = new Product("Moisturizer3", 52, 1);
let product4 = new Product("Moisturizer4", 53, 1);

console.log("Available Items: ");
console.log(product1);
console.log(product2);
console.log(product3);
console.log(product4);

function Customer(name, cart, addToCart, removeToCart){
        this.name = name;
        this.cart = [];
        this.addToCart=function(product){
            this.cart.push(product);
            return `${product.name} added to cart,`
        };
        this.removeToCart=function(product){
            let check = this.cart.includes(product);
            if(check){
                let productIndex = this.cart.indexOf(product);
                let removedItem = this.cart.splice(productIndex,1);
                return `${product.name} is removed from the cart.`;
            }else{
                return `${product.name} is not in the cart.`;
            }
        };
}


// Instance of a customer
let ayin = new Customer();
// Adding products to cart
console.log("Adding items to the cart!");
console.log(ayin.addToCart(product4));
console.log(ayin.addToCart(product3));
console.log(ayin.addToCart(product2));
console.log(ayin.addToCart(product1));

console.log("Item's in customer's cart: ");
console.log(ayin.cart);
console.log("Removing an item in the cart: ");
console.log(ayin.removeToCart(product3));
console.log("Updated items in customer's cart: ");
console.log(ayin.cart);
console.log("Additional checking if item was present in the cart");
console.log(ayin.removeToCart(product3));
