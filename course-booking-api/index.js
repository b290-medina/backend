const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");

// Allows access to routes defined within our application
const userRoute = require("./routes/userRoute");
const courseRoute = require("./routes/courseRoute");

const app = express();

// Connect MongoDB Database
mongoose.connect("mongodb+srv://admin:admin123@zuitt.7kyra1x.mongodb.net/booking-DB?retryWrites=true&w=majority", 
		{
			useNewUrlParser	: true,
			useUnifiedTopology	: true
		}
);

mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."));

// Middlewares
// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended : true }));

// Routes
// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoute);
// Defines the "/courses" string to be included for all course routes defined in the "course" route file
app.use("/courses", courseRoute);


if(require.main === module){
	app.listen(process.env.PORT || 4000, () => {
		console.log(`API is now online on port ${process.env.PORT || 4000}`)
	});
}

module.exports = { app, mongoose };