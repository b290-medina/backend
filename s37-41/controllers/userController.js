const bcrypt = require("bcrypt")

// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require("../models/User");
const Course = require("../models/Course");
const auth = require("../auth.js");
// Check if the email already exists
/*
	Steps: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email:reqBody.email}).then(result=>{
		if(result.length>0){
			return true;
		}else{
			return false;
		}
	}).catch(err=>{
		console.log(err);
		return false;
	})
};

// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/
module.exports.registerUser = (reqBody) => {
	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password : bcrypt.hashSync(reqBody.password, 10),
		mobileNo : reqBody.mobileNo		
	});
	// Save the created object to our database
	return newUser.save()
		.then(user => {return true}).catch(err => {return false});
}


// User authentication (login)
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/
module.exports.loginUser = (reqBody) =>{
	return User.findOne({email:reqBody.email}).then(result=>{
		// User does not exist
		if(result == null){
			console.log("User not registered")
			return false; //User is not registered
		// User exists
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// passwords match
			if(isPasswordCorrect){
				return {access : auth.createAccessToken(result)}
			// password didnt match
			}else{
				console.log("Password did not match")
				return false; // Password didnt match
			}
		}
	}).catch(err=>{
		console.log(err);
		return false;
	})
}

// User retrieval
module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.userId).then(result=>{
		result.password = "";
		return result;
	}).catch(err=>{
		console.log(err);
		return false;
	})
};

// Enroll user to a class
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/
// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user

module.exports.enroll = async(data)=>{


	// Add the course ID in the enrollments array of the user
	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend
	let isUserUpdated = await User.findById(data.userId)

		// Adds the courseId in the user's enrollments array
		.then(user => {
			user.enrollments.push({courseId : data.courseId});
			// Saves the updated user information in the database
			return user.save()
				.then(user=>true)
					.catch(err=>{
						console.log(err);
						return false;
					});
		}).catch(err => {
			console.log(err);
			console.log("Error in retrieving");
			return false
		});

	let isCourseUpdated = await Course.findById(data.courseId)
		.then(course=>{
			course.enrollees.push({userId:data.userId});
			return course.save()
				.then(course => true)
					.catch(err => {
						console.log(err);
						return false
					});
		}).catch(err => {
				console.log(err);
				console.log("Error in retrieving");
				return false
			});
		// Condition that will check if the user and course documents have been updated
		// User enrollment successful
		return (isUserUpdated&&isCourseUpdated) ? true : false;
}