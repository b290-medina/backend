const express = require("express");
const mongoose = require("mongoose")
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings		
const cors = require("cors")
const app = express()

// Allows access to routes defined within our application
const userRoute = require("./routes/userRoute.js")
const courseRoute = require("./routes/courseRoute.js")

// connect mongoDB database
mongoose.connect("mongodb+srv://admin:admin123@zuitt.7kyra1x.mongodb.net/booking-DB?retryWrites=true&w=majority",
	{
		useNewUrlParser : true, 
		useUnifiedTopology: true
	}
);

let db = mongoose.connection
db.once("open", ()=>console.log("Now connected to MongoDB atlas"));

// Setup middlewares
// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Routes
// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoute);
// Defines the "/courses" string to be included for all course routes defined in the "course" route file
app.use("/courses", courseRoute);


if(require.main===module){
	app.listen(process.env.PORT || 4000, () =>{
		console.log(`API is now online on port ${process.env.PORT || 4000}`)
	})
}
module.exports = {app, mongoose};
