const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseController");
const auth = require("../auth.js");


// Route for creating a course by admins
router.post("/", auth.verify,(req, res)=>{
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin){
		courseController.addCourse(req.body)
		.then(resultFromController => res.send(resultFromController))
	}else{
		res.send("User is not an admin");
	}
});

router.get("/all", auth.verify, (req, res) => { //need for middleware
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		courseController.getAllCourses()
		.then(resultFromController => res.send(resultFromController))
	}else{
		res.send("User is not an admin")
	}
	
});

// Route for retrieving all the ACTIVE courses
// Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the courses
router.get("/", (req, res)=>{
	courseController.getAllActiveCourses()
		.then(resultFromController=>res.send(resultFromController))
});

// Route for retrieving a specific course
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
router.get("/:courseId", (req, res)=>{
	console.log(req.params);
	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
	// Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e
	// The course Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "courseId" in the route
	courseController.getCourse(req.params)
		.then(resultFromController => res.send(resultFromController))
});

// Route for updating a course
// JWT verification is needed for this route to ensure that a user is logged in before updating a course

router.put("/:courseId", auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin){
		courseController.updateCourse(req.params, req.body)
		.then(resultFromController =>res.send(resultFromController))
	}else{
		console.log("Unauthorized access")
		res.send(false)
	}
});

// Route for archiving a course
router.patch("/:courseId/archive", auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin){
		courseController.archiveCourse(req.params, req.body)
		.then(resultFromController =>res.send(resultFromController))
	}else{
		console.log("Unauthorized access")
		res.send(false)
	}
});

module.exports = router