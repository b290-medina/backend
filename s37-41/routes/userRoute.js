const express = require('express')
const router = express.Router();


const auth = require("../auth.js");
const userController = require("../controllers/userController.js")
// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" property of our "request" object to the corresponding controller function
router.post("/checkEmail", (req, res)=>{
	userController.checkEmailExists(req.body)
		.then(resultFromController => res.send(resultFromController));
});

// Router for user registration
router.post("/register",(req, res)=>{
	userController.registerUser(req.body)
		.then(resultFromController => res.send(resultFromController));
});

// Router for login
router.post("/login", (req, res)=>{
	userController.loginUser(req.body)
		.then(resultFromController => res.send(resultFromController))
});

// Router for retrieving user details
// The "auth.verify" acts as a middleware to ensure that the user is logged in before they can access user details
router.post("/details", auth.verify,(req, res)=>{
	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId : userData.id})
		.then(resultFromController => res.send(resultFromController))
});


// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId : userData.id,
		courseId : req.body.courseId
	}
	userController.enroll(data)
		.then(resultFromController => res.send(resultFromController));
})









// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;