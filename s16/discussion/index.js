// console.log("Hello");

// [SECTION] Arithmetic operators
	let x = 1397;
	let y = 7831;
	let sum = x+y;
	let difference = x-y;
	let product = x*y;
	let quotient = x/y;
	let modulus = x%y;

	console.log("Result of addition operator: " + sum);
	
	// Mini activity

	console.log("Result of subtraction operator: " + difference);
	console.log("Result of multiplication operator: " + product);
	console.log("Result of division operator: " + quotient);
	console.log("Result of modulus operator: " + modulus);

// [SECTION] Assignment Operators	
	// Basic Assignment Operator (=)
	// The assignment operator assigns the value of the **right** operand to a variable.
	let assignmentNumber = 8;

	// Addition Assignment operator (+=)
	// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.
	// assignmentNumber + assignmentNumber = 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	// Shorthand for assignmentNumber + assignmentNumber = 2; 
	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	// Mini activity

	assignmentNumber -= 2;	
	console.log("Result of subtraction assignment operator: " + assignmentNumber);
	assignmentNumber *= 2;	
	console.log("Result of multiplication assignment operator: " + assignmentNumber);
	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " + assignmentNumber);
	assignmentNumber %= 2;
	console.log("Result of modulo assignment operator: " + assignmentNumber);


	// Multiple Operators and Parentheses
	/*
		- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
        - The operations were done in the following order:
            1. 3 * 4 = 12
            2. 12 / 5 = 2.4
            3. 1 + 2 = 3
            4. 3 - 2.4 = 0.6
	*/

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operation: " + mdas);

	let pemdas = 1 + (2-3) * (4/5);	
	console.log("Result of pemdas operation: " + pemdas);
	/*
	    - By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
	    - The operations were done in the following order:
	        1. 4 / 5 = 0.8
	        2. 2 - 3 = -1
	        3. -1 * 0.8 = -0.8
	        4. 1 + -.08 = .2
	*/

	pemdas = (1 + (2-3)) * (4/5);	
	console.log("Result of pemdas operation: " + pemdas);

	//  Increment and Decrement
	// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

	let z = 1;

	// The value of "z" is added by a value of one before returning the value and storing it in the variable "increment	
	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	// The value of "z" was also increased even though we didn't implicitly specify any value reassignment
	console.log("Result of pre-increment; " + z );


	// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one
	increment = z++;
	// The value of "z" is at 2 before it was incremented
	console.log("Result of post-increment; " + increment);
	// The value of z was increased again reassigning the value to 3
	console.log("Result of post-increment; " + z );

	// The value of "z" is decreased by a value of one before returning the value and storing it in the variable "decrement"
	let decrement = --z;
	// The value of "z" is at 3 before it was decremented
	console.log("Result of pre-decrement: " + decrement);
	// The value of "z" was decreased reassigning the value to 2
	console.log("Result of pre-decrement; " + z );	

	// The value of "z" is returned and stored in the variable "increment" then the value of "z" is decreased by one
	decrement = z--;
	// The value of "z" is at 2 before it was decremented
	console.log("Result of post-decrement; " + decrement);
	// The value of "z" was decreased reassigning the value to 1
	console.log("Result of post-decrement; " + z );


// [SECTION] Comparison Operators

	let juan = "juan";

	// (loose) Equality operator
	/* 
        - Checks whether the operands are equal/have the same content
        - Attempts to CONVERT AND COMPARE operands of different data types
        - Returns a boolean value
    */

    console.log(1 == 1);
    console.log(1 == 2);
    console.log(1 == '1');
    console.log(0 == false);
    console.log("juan"=="juan");
    console.log("juan"==juan);

    // Strict Equality Operator

    /* 
        - Checks whether the operands are equal/have the same content
        - Also COMPARES the data types of 2 values
        - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
        - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
        - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
        - Strict equality operators are better to use in most cases to ensure that data types provided are correct
    */

    console.log(1 === 1);
    console.log(1 === 2);
    console.log(1 === '1');
    console.log(0 === false);
    console.log("juan"==="juan");
    console.log("juan"===juan);

    // (loose) Inequality operator

    /* 
		- Checks whether the operands are not equal/have different content
		- Attempts to CONVERT AND COMPARE operands of different data types
	*/


    console.log(1 != 1);
    console.log(1 != 2);
    console.log(1 != '1');
    console.log(0 != false);
    console.log("juan"!="juan");
    console.log("juan"!=juan);

    //  (strict	) Inequality Operator
    /* 
        - Checks whether the operands are not equal/have the same content
        - Also COMPARES the data types of 2 values
    */

	console.log(1 !== 1);
    console.log(1 !== 2);
    console.log(1 !== '1');
    console.log(0 !== false);
    console.log("juan"!=="juan");
    console.log("juan"!==juan);


// [SECTION] Relational Operators


   let a  = 50;
   let b = 65;

   // GT or Greater Than operator (>)
   let isGreaterThan = a>b;
   console.log(isGreaterThan);
   // LT or Less Than operator (<)
   let isLessThan = a<b;
   console.log(isLessThan);
   // GTE or Greater Than or Equal operator (>=)
   let isGTorEqual = a>=b;
   console.log(isGTorEqual);
   // LTE or Greater Than or Equal operator (<=)
   let isLTorEqual = a<=b;
   console.log(isLTorEqual);


   let numStr = '30';
   console.log(a>numStr); //true - concerted string to a number
   console.log(b<=numStr);

   let str = "twenty";
   console.log(b>=str); //false - str resulted to NaN string is not number


// [SECTION] Logical Operator

   let isLegalAge = true;
   let isRegistered = false;

   // AND operator (&&)
   // Returns true if all operands are true
   let allRequirementsMet = isLegalAge && isRegistered;
   console.log("Result of AND operator: " +allRequirementsMet);

   // OR operator (||)
   // Returns true if one of the operands are true
   let someRequirementsMet = isLegalAge || isRegistered;
   console.log("Result of OR operator: " + someRequirementsMet);

   // NOT Operator (!)
   // Returns the opposite value

   let someRequirementsNotMet = !isRegistered;
   console.log("Result of NOT operator: " + someRequirementsNotMet);
