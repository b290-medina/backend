// Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (browser) and servers (node JS/express JS applications) communicate by exchanging individual messages.
// The messages sent by the client, usually a Web browser, are called requests
// The messages sent by the server as an answer are called responses.
const http = require("http");
const port = 4000;

const app = http.createServer((request, response)=>{
	// The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter
	// The method "GET" means that we will be retrieving or reading information		
	if(request.url=="/items" && request.method == "GET"){
		// Requests the "/items" path and "GETS" information
		response.writeHead(200, {"Content-Type":"text/plain	"});
		// Ends the response process
		response.end("Data retrieved from the database!");
	};
	// The method "POST" means that we will be adding or creating information
	if(request.url=="/items" && request.method == "POST"){
		// requests the items path and "SENDS" information
		response.writeHead(200, {"Content-Type":"text/plain	"});
		response.end("Data to be sent to the database!");
	}
});

app.listen(port, ()=>console.log(`Server running at localhost:${port}`));