// CRUD Operations

// [SECTION] Inserting documents (CREATE)

// Inserting a single document
// Syntax db.collectionName.insertOne({object});
db.users.insertOne({
	firstName : "Jane",
	lastName : "Doe",
	age: 21,
	contact:{
		phone : "09123456789",
		email : "janedoe@gmail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
});

// Inserting multiple documents
// Syntax db.collectionName.insertMany([{objectA},{objectB}]);
db.users.insertMany([{
	firstName : "Stephen",
	lastName : "Hawking",
	age: 76,
	contact:{
		phone : "09123456780",
		email : "stephenhawking@gmail.com"
	},
	courses: ["Python", "React", "PHP"],
	department: "none"
},
{
	firstName : "Nail",
	lastName : "Armstrong",
	age: 82,
	contact:{
		phone : "09123456781",
		email : "neailarmstrong@gmail.com"
	},
	courses: ["React", "Laravel", "Saas"],
	department: "none"
}]);

// [SECTION] finding documents (RETRIEVE)
/*
	Syntax  db.collectionName.find();
			db.collectionName.find({field: value});
			db.collectionName.findOne({field: value}); 
				If multiple documents match the criteria for finding a document, only the FIRST document that matches will be returned
*/

// Leaving the search criteria empty will retrieve all the documents
db.users.find();
db.users.find({firstName:"Stephen"});

// Finding documents with multiple parameters
// Syntax db.collectionName.find({fieldA: valueA, fieldB : valueB});
db.users.find({lastName: "Armstrong", age: 82});

// [SECTION] Updating documents (Update)
// updating a single document 
// Syntax db.collectionName.updateOne({criteria}, {$set {field:value}});


// Document to be updated
db.users.insertOne({
	firstName : "Test",
	lastName : "Test",
	age: 0,
	contact:{
		phone : "00000000000",
		email : "test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.updateOne(
	{firstName : "Test"},
	{
		$set : {
			firstName : "Bill",
			lastName : "Gates",
			age: 65,
			contact:{
				phone : "09123456783",
				email : "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
)

// Updating multiple documents
/*
	Syntax:
		db.collectionName.updateMany({criteria},  {$set: {fieldA:valueA}})
*/
db.users.udpateMany(
	{department:"none"},
	{
		$set: {department:"HR"}
	}
)

// Replace One
// Can be used if replacing the whole document is needed
db.users.replaceOne(
	{firstName:"Bill"},
	{
		firstName : "Bill",
			lastName : "Gates",
			age: 65,
			contact:{
				phone : "09123456783",
				email : "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations"
	}
)

// [SECTION] Deleting documents	(Delete)

// Documents to be deleted

db.users.insertOne({
	firstName:"test"
});

// deleting a single document
/*
	Syntax:
	db.collectionName.deleteOne({criteria})
*/

db.users.deleteOne({
	firstName:"test"
});

// Deleting Many
/*
	Syntax:
	db.collectionName.deleteMany({criteria})
*/

db.users.deleteMany({
	firstName:"Bill"
});

// [SECTION] advanced queries

db.users.find({
	contact:{
		phone:"09123456780",
		email: "stephenhawking@gmail.com"
	}
});

db.users.find({"contact.email":"janedoe@gmail.com"});

// Query an array with exact elements
db.users.find({courses:["CSS", "Javascript", "Python"]});
// Query an array without regards to the element order
db.users.find({courses:{$all: ["React","Python"]}});

// Querying an embedded array
db.users.insertOne({
	namearr:[
		{
			nameA:"juan"
		},
		{
			nameB: "tamad"
		}
	],
});

db.users.find({
	namearr:{
		nameA:"juan"
	}
});