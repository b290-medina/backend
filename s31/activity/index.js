//Add solution here

const http = require("http");
const port = 3000;
const app = http.createServer((req,res)=>{
	if(req.url=="/login"){
		res.writeHead(200,{"Content-Type" : "text/plain"});
		res.end("Welcome to the login page.");
	}else{
		res.writeHead(404,{"Content-Type" : "text/plain"});
		res.end("I'm sorry the page you are looking for cannot be found.");
	}
});

// Uses the "app" and "port" variables created above
app.listen(port);
console.log(`Server now runnning at localhost:${port}`); 

//Do not modify
module.exports = {app}