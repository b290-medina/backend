// Use require directive to load Node.js
// A "package" or "module" is a software component or part of a program that contains one or more routine
// The "http module" lets node.js data using HYper Text Transfer Protocol
// Clients (browser) and servers (node/express) communicate by exchanging individual messages.

let http = require("http");

// Using this module "createServer()" method, we can create an HTTP server that listens o specified port.
//  A port is a virtual point where network connection start and end.
// Each port is associated with specific process or server
http.createServer(function (request, response) {
	// writeHead() method
	// Set the status code for the response - 200 -> OK/SUCCESS

	response.writeHead(200, { "Content-Type" : "text/plain" });

	// Send the response with text content "Hello World!"
	response.end("Hello World!");
}).listen(4000);

// Whenever the server starts, console will print the message in our "terminal"
console.log("Server running at localhost:4000");